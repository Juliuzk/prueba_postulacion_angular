import { Component, OnInit, ɵConsole } from '@angular/core';
//Importacion de librerias para uso de get
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-uno',
  templateUrl: './uno.component.html',
  styleUrls: ['./uno.component.css']
})
export class UnoComponent implements OnInit {

  //ARRAY DE OBJETO CON LOS NUMEROS YA PROCESADOS
  dataProcesada: any[] = []
  //ARRAY CON LOS NUMEROS RECUPERADOS
  dataRecuperada: any[] = []
  //ARRAY CON LOS NUMEROS ORDENADOS DE MENOR A MAYOR
  dataOrdenada: any[] = []
  //OBJETO CON NUMERO Y CANTIDAD
  solucion: any = { }
  //DISABLED BOOLEAN PARA ACTIVAR Y DESACTIVAR EL BOTON DESPUES DE CADA CLICK
  disabled: boolean = false

  constructor(private http: HttpClient) {
  
  }

  ngOnInit() {
  }

  //Creamos un metodo para que al apretar el boton dispare nuestra funcion que traera y procesara la informacion
  traerDatos() {

    // CADA VES QUE SE DA CLICK EN CONSUMIR API, RESETEAMOS NUESTRO ARRAY PARA GENERAR DENUEVO LA TABLA
    this.solucion = []
    this.dataProcesada.splice(0, this.dataProcesada.length)
    this.disabled = true
    // LUEGO DE VACIADA LA TABLA, HACEMOS EL LLAMADO A LA API Y PROCESAMOS SU INFORMACION
    this.http.get('http://patovega.com/prueba_frontend/array.php').subscribe((res: any) => {
        //Primero guardamos en un objeto la cantidad de repeticiones que tenemos de cada numero
        for (var i = 0; i < res['data'].length; ++i) {
          if (!this.solucion[res['data'][i]]) {
            this.solucion[res['data'][i]] = 0;
          }
          ++this.solucion[res['data'][i]];
        }

        this.dataRecuperada = res['data'];
        console.log(this.dataRecuperada)
        //REALIZAMOS UN CODIGO BASICO DE ORDENAMIENTO BURBUJA, PARA PODER ORDENAR NUESTRO ARRAY
        //GUARDANDO LOS NUMEROS ORDENADOS EN DATAORDENADO Y UN AUXILIAR QUE NOS PERMITIRA INTERCAMBIAR LOS NUMEROS
        this.dataOrdenada = res['data'].slice();

        let aux = 0;

        for (let i = 0; i < this.dataOrdenada.length; i++) {
            for (let j = 1; j < (this.dataOrdenada.length - i); j++) {
                if (this.dataOrdenada[j - 1] > this.dataOrdenada[j]) {
                  aux = this.dataOrdenada[j - 1];
                  this.dataOrdenada[j - 1] = this.dataOrdenada[j];
                  this.dataOrdenada[j] = aux;
                }
            }
        }

        for (let aux in this.solucion) {
          let auxData = {
            numero: null,
            ocurrencias: null,
            primera_posicion: null,
            ultima_posicion: null
          }

          auxData.numero = Number(aux);
          //this.solucion[aux] nos devuelve la cantidad de veces que aparece el numero pero no las ocurrencias de este
          //por lo cual le restamos 1
          auxData.ocurrencias = this.solucion[aux] - 1;
          auxData.primera_posicion = res['data'].indexOf(Number(aux));
          auxData.ultima_posicion = res['data'].lastIndexOf(Number(aux))

          this.dataProcesada.push(auxData)

        }
        console.log(this.dataProcesada)
        this.disabled = false
    });

  
  }



}
