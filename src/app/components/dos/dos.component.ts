import { Component, OnInit } from '@angular/core';
//Importacion de librerias para uso de get
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-dos',
  templateUrl: './dos.component.html',
  styleUrls: ['./dos.component.css']
})
export class DosComponent implements OnInit {

  //ARRAY DONDE SE GUADARAN LOS PARRAFOS RECUPERADOS
  dataRecuperada: any[] = []
  //ARRAY DONDE SE GUARDARAN LA INFORMACION DE LAS LETRAS
  dataLetras: any[] = []
  //ARRAY DONDE SE GUARDARA LA INFORMACION DE LOS NUMEROS
  dataNum: any[] = []
  //BOOLEAN DISABLED PARA ACTIVAR Y DESACTIVAR EL BOTON DESPUES DEL PRIMER CLICK
  disabled: boolean = false
  //ARRAY DE OBJETOS CON LAS RESPUESTAS
  dataResultante: any[] = []

  constructor(private http: HttpClient) {

  }

  ngOnInit() {
  }

  //Creamos un metodo para que al apretar el boton dispare nuestra funcion que traera y procesara la informacion
  async traerDatos() {
    //le cambiamos al valor a disabled para que desabilite el boton consumir api hasta que ya se haya terminado todo el proceso
    this.disabled = true

    //reseteamos nuestros array para realizar todo denuevo al cliquear el boton
    this.dataRecuperada.splice(0, this.dataRecuperada.length)
    this.dataLetras.splice(0, this.dataLetras.length)
    this.dataResultante.splice(0, this.dataResultante.length)

    const r = this.http.get('http://patovega.com/prueba_frontend/dict.php').subscribe((res: any) => {

      res['data'] = JSON.parse(res['data']);


      //PASAREMOS TODOS NUESTRO DATA RECUPERADO A MINUSCULAS PARA COMENZAR A LIMPIAR DEJANDO SOLO LO QUE NOS INTERESE
      for (let i = 0; i < res['data'].length; i++) {
        //texto original
        this.dataRecuperada.push(res['data'][i]['paragraph'])

        res['data'][i]['paragraph'] = res['data'][i]['paragraph'].toLowerCase()

        this.dataLetras.push(res['data'][i]['paragraph'].replace(/[^a-z]+/g, ' '))
      }

      for (let i = 0; i < this.dataLetras.length; i++) {
        let auxData: any = {

        }
        auxData = this.contarCaracteres(this.dataLetras[i])

        this.dataResultante.push(auxData)
      }

      // En este punto tenemos realizado el conteo de caracteres, por lo cual debemos realizar la suma de los numeros segun corresponda
      // PASO 1: Dejaremos solamente los numeros que se encuentren, cambiando todo lo que no sea numeros a " "

      // Para generar un array con numeros a partir del string que tenemos
      // usaremos expresiones regulares
      // var test=("1,005.")
      // console.log(test.match(/(\d+)/g)); 
      // ---->["1", "005"]
      // var test=("-15..")
      // console.log(test.match(/(\d+)/g)); 
      // ---> ["15"]

      //Ya con nuestra expresion regular y nuestra funcion podemos comenzar a 
      for (let i = 0; i < this.dataResultante.length; i++) {
        //obtenemos los numeros de nuestro array
        let num = this.dataRecuperada[i].match(/(\d+)/g);
        if (num != null) {
          var aux = 0
          for (let i of num) {
            // debemos cambiar a entero ya que los numeros obtenidos eran string
            aux += parseInt(i)
          }
        } else {
          aux = 0
        }

        this.dataResultante[i].suma = aux
      }

      //habilitamos nuevamente nuestro botton
      this.disabled = false
    });
    
  }

  //Este metodo recibe un string con caracteres y devuelve el conteo de los caracteres en un objeto
  contarCaracteres(str) {
    //Este metodo permite reaalizar el conteo de los caracteres que estan dentro de un string, guardando estos en un objeto creado
    //para este problema

    let cantidadLetras: any = {
      a: 0,
      b: 0,
      c: 0,
      d: 0,
      e: 0,
      f: 0,
      g: 0,
      h: 0,
      i: 0,
      j: 0,
      k: 0,
      l: 0,
      m: 0,
      n: 0,
      ñ: 0,
      o: 0,
      p: 0,
      q: 0,
      r: 0,
      s: 0,
      t: 0,
      u: 0,
      v: 0,
      w: 0,
      x: 0,
      y: 0,
      z: 0,
    }
    
    let caracteres = str.toLowerCase().replace(/ /g, '').split('').filter((c, i, self) => (
      self.indexOf(c) === i
    ))

    for (var i = 0; i < caracteres.length; i++) {
      let arreglo = []
      str.split('').map(n => {
        if (n.toLowerCase() === caracteres[i]) {
          arreglo.push(n)

        }
      })
      // console.log('push: ' + caracteres[i] +" , " + arreglo.length)
      cantidadLetras[caracteres[i]] = arreglo.length
      // cantidadLetras.push({ [caracteres[i]]: arreglo.length })
    }
    return cantidadLetras
  }

}
