import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// Importacion libreria para rutas
import { RouterModule, Routes} from '@angular/router';
// Importacion libreria http
import { HttpClientModule } from "@angular/common/http";
//Componentes
import { NavbarComponent } from './components/navbar/navbar.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { UnoComponent } from './components/uno/uno.component';
import { DosComponent } from './components/dos/dos.component';
import { FooterComponent } from './components/footer/footer.component';


const routes: Routes = [

  {path:'' , component: InicioComponent, pathMatch: 'full'},
  {path:'uno' , component: UnoComponent},
  {path:'dos' , component: DosComponent},
  //En caso de que se entre a una url que no esta definida redireccionamos a nuestro inicio
  {path:'**' , redirectTo:'/', pathMatch: 'full'}

];


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    InicioComponent,
    UnoComponent,
    DosComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // Importamos rutas
    RouterModule.forRoot(routes),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
